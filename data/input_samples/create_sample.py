import numpy as np
import pandas as pd

aux = pd.read_csv ('archives/kepler_km_clean_sample.csv', index_col=0)
index = aux.index
df = pd.read_csv ('full_training_set.csv', index_col=0)
df = df.loc[np.intersect1d (index, df.index)]
aux = pd.read_csv ('filter_training_set.csv', index_col=0)
df = df.join (aux['label'], lsuffix='_rot', rsuffix='_filter')
df.to_csv ('kepler_km_sample.csv')


