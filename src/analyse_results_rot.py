import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import os
from os import path
import sys 
from astropy.io import fits
import glob

abspath = path.abspath ('..')
path_out = path.join (abspath, 'data/summaries/')

analyse_filter=False
os.chdir (path.join(abspath,'data/results_training/rotation/'))

list_results = glob.glob ('run*.csv')
df = pd.DataFrame ()

for elt in list_results :
  df_aux = pd.read_csv (elt, index_col=0)
  df = pd.concat ([df, df_aux])

dfw = df.loc[df['label']!=df['label_pred']]
summary = df.groupby (df.index).mean()
fileref = path.join (abspath, 'data/input_samples/kepler_km_sample.csv') 
ref = pd.read_csv (fileref, index_col=0) 
summary = summary.join (ref['label_rot'])
summary['label_pred'] = 'norot'
summary.loc[summary['Prot']>=0.5,'label_pred'] = 'Prot'

summary.to_csv (path.join (path_out, 'summary_rotation.csv'))

count = dfw.index.value_counts().to_frame ()
count = count.rename (columns={0:'misclassified'})
count = count.join (df.index.value_counts().to_frame())
count = count.rename (columns={0:'drawn'})
count['ratio'] = count['misclassified'] / count['drawn'] 

print ('Number of stars in the sample', summary.index.size)
print ('Number of stars misclassified at least once:', count.loc[count['ratio']>0].index.size)
print ('Number of stars misclassified half of the time:', count.loc[count['ratio']>0.5].index.size)
print ('Number of stars always misclassified:', count.loc[count['ratio']==1].index.size)

print ('Number of stars misclassified (global):', summary.loc[summary['label_pred']!=summary['label_rot']].index.size)


print ('Accuracy:', summary.loc[summary['label_pred']==summary['label_rot']].index.size / summary.index.size)


print ('Number of stars to visually check', summary[(summary['Prot']>0.4)&(summary['Prot']<0.8)].index.size)
print ('Number of stars corrected by the visual check', 
       summary[((summary['Prot']>0.4)&(summary['Prot']<0.8))&(summary['label_pred']!=summary['label_rot'])].index.size)



print ('Fraction of stars to visually check', summary[(summary['Prot']>0.4)&(summary['Prot']<0.8)].index.size / summary.index.size)

print ('Accuracy after visual checks', 
       summary[((summary['Prot']>0.4)&(summary['Prot']<0.8))|(summary['label_pred']==summary['label_rot'])].index.size / summary.index.size)



outliers = count.loc[count['ratio']>0.5]
outliers.to_csv (path.join (abspath, 'data/input_samples/outliers_rot.csv'))

visual_check = summary.loc[(summary['Prot']>0.4)&(summary['Prot']<0.8)].index
np.savetxt (path.join (abspath, 'data/summaries/kic_visual_check_rot.dat'), visual_check)

os.chdir (path.join (abspath,'src/'))


