import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from finding_harmonics import find_harmonic_nc 
from flag_filter import flag_filter
from os import path

'''
Flag check list
-2 : Large tolerance zone (30%) (no check but to see which number of them)
-1 : Prot corrected by reattrbuting filter (no check needed but I keep track)
0 : no check needed
1 : filter
2 : harmonics
3 : instrumental modulation and long trend (Prot>38 days)
4 : quarters
5 : short trend
6 : CPCB with Prot > 7 days
'''

abspath = path.abspath ('..') 

df = pd.read_csv (path.join (abspath, 'data/summaries/summary_filter.csv'), index_col=0)
cpcb = pd.read_csv (path.join (abspath, 'data/summaries/summary_cpcb.csv'), index_col=0)
rot = pd.read_csv (path.join (abspath, 'data/summaries/summary_rotation.csv'), index_col=0)

ref = pd.read_csv (path.join (abspath, 'data/input_samples/kepler_km_sample.csv'), index_col=0)
flag = pd.read_csv (path.join (abspath, 'data/input_samples/kepler_km_full_flag.csv'), index_col=0)
quarters = pd.read_csv (path.join (abspath, 'data/input_samples/kepler_nquarters.csv'), index_col=0)

df = df.join (ref, rsuffix = '_ref')
df = df.join (flag['cpcb1_flag'])
df = df.join (cpcb['label_pred'], rsuffix='_cpcb')
df = df.join (quarters['n_quarters'])
df = df.join (rot[['label_rot', 'label_pred']], rsuffix='_rot')
df['flag_check'] = 0


correc_angela = [3837321, 9593950, 10018467, 10614279, 10843431]
slight_correc_angela = [3550041, 3555898, 3634196, 3834448, 4648984, 4908380, 
                        4912212, 5349936, 6626854, 7661316, 8617364, 9074930, 
                        9221129, 9414380, 9414380, 9699942, 10064860, 10676216, 
                        10753707, 11921871, 12006469, 12215093]

out = np.abs(df['Prot_pred']-df['Prot'])>0.1*df['Prot']
outliers = df.loc[out]
outliers.to_csv (path.join (abspath, 'data/input_samples/outliers_mlprot.csv'))
print ('Total number of stars', df.index.size)
print ('Outliers before reattributing filters', df.loc[out].index.size)
ml_good = np.setdiff1d (df.index, out.index)

# 30 % 'large tolerance zone'
thin = df.loc[np.abs(df['Prot']-df['Prot_pred'])<0.1*df['Prot']] 
large = df.loc[np.abs(df['Prot']-df['Prot_pred'])<0.3*df['Prot']] 
df.loc[np.setdiff1d (large.index, thin.index), 'flag_check'] = -2




#short lightcurves
df.loc[df['n_quarters']<5, 'flag_check'] = 4

#short trend
df.loc[df['Prot_pred']<1.6, 'flag_check'] = 5

#cpcb with prot > 7 days
df.loc[(df['label_pred_cpcb']==1)&(df['Prot_pred']>7), 'flag_check'] = 6

#Filter reattribution
thresh = 0.15
df = flag_filter (df, thresh=thresh, only_gwps=False)

out = np.abs(df['Prot_pred']-df['Prot'])>0.1*df['Prot']
print ('Outliers after reattributing filters', df.loc[out].index.size)

#harmonic selection
s = find_harmonic_nc (df, low=1.7, up=2.3, use_all_filter=False)
thresh = 0
df.loc[s>thresh, 'flag_check'] = 2


#instrumental modulation
df.loc[df['Prot_pred']>38, 'flag_check'] = 3


print ('Number of stars flagged for harmonics', df.loc[df['flag_check']==2].index.size) 
print ('Number of stars flagged for harmonics and corrected', df.loc[(df['flag_check']==2)&(np.abs(df['Prot_pred']-df['Prot'])>0.1*df['Prot'])].index.size) 

print ('Number of stars flagged for filters', df.loc[df['flag_check']==1].index.size) 
print ('Number of stars flagged for filters and corrected', df.loc[(df['flag_check']==1)&(np.abs(df['Prot_pred']-df['Prot'])>0.1*df['Prot'])].index.size) 

print ('Number of stars flagged for short lightcurves', df.loc[df['flag_check']==4].index.size) 
print ('Number of stars flagged for short lightcurves and corrected', df.loc[(df['flag_check']==4)&(np.abs(df['Prot_pred']-df['Prot'])>0.1*df['Prot'])].index.size) 


print ('Non checked stars in the large tolerance zone', df.loc[df['flag_check']==-2].index.size)
print ('Total number of visual checks to perform', df.loc[df['flag_check']>0].index.size)
print ('Fraction of visual checks to perform', df.loc[df['flag_check']>0].index.size /df.index.size)


print ('Number of correct stars in RotClass & PeriodSel before visual checks:', df.loc[(np.abs(df['Prot_pred']-df['Prot'])<0.1*df['Prot'])&(df['label_rot']==df['label_pred_rot'])].index.size)
print ('Corresponding fraction:', 
       df.loc[(np.abs(df['Prot_pred']-df['Prot'])<0.1*df['Prot'])&(df['label_rot']==df['label_pred_rot'])].index.size / df.index.size)

good = (np.abs(df['Prot_pred']-df['Prot'])<0.1*df['Prot'])|(df['flag_check']>0)


print ('Number of flagged stars that will be corrected', df.loc[out&(df['flag_check']>0)].index.size)
print ('Number of stars with correct rotation period after visual checks', df.loc[good].index.size)
print ('Final accuracy of PeriodSel', df.loc[good].index.size/df.index.size * 100)



vc_rot = np.loadtxt (path.join (abspath, 'data/summaries/kic_visual_check_rot.dat'))
vc_period = df.loc[df['flag_check']>0].index
vc = np.concatenate ((vc_rot, vc_period))

df['rot_check'] = 0
df.loc[np.intersect1d(df.index, vc_rot), 'rot_check'] = 1


print ('Total number of visual checks on the two steps (RotClass & PeriodClass):', np.unique (vc).size)
summary_rot = pd.read_csv (path.join (abspath, 'data/summaries/summary_rotation.csv'), index_col=0)
print ('Corresponding fraction:', np.unique (vc).size / summary_rot.index.size)


print ('Number of correct stars in RotClass & PeriodSel after visual checks:', 
       df.loc[good&((df['label_rot']==df['label_pred_rot'])|(df['rot_check']==1))].index.size)
print ('Corresponding fraction:', df.loc[good&((df['label_rot']==df['label_pred_rot'])|(df['rot_check']==1))].index.size / df.index.size)


plt.rcParams['legend.fontsize'] = 10
plt.ion ()

fig = plt.figure (figsize=(12,12), num=1)
ax = fig.add_subplot (111)
ax.set_xlabel (r'$P_\mathrm{rot,S19}$ (days)', fontsize=25)
ax.set_ylabel (r'$P_\mathrm{rot,ML}$ (days)', fontsize=25)

ax.fill_between ([0,150], [0,135], [0,165], alpha=0.3, color='blue')
ax.fill_between ([0,150], [0,165], [0,195], alpha=0.1, color='green')
ax.fill_between ([0,150], [0,135], [0,105], alpha=0.1, color='green')


s = 40

ax.scatter(df.loc[df['flag_check']==0, 'Prot'], df.loc[df['flag_check']==0, 'Prot_pred'], marker='.', s=s, color='black', label=None)
#ax.scatter(df.loc[df['flag_check']==-2, 'Prot'], df.loc[df['flag_check']==-2, 'Prot_pred'], marker='o', s=30, color='peru', linewidths=0.5, edgecolors='black', label='Large tolerance zone')

ax.scatter(df.loc[df['flag_check']==2, 'Prot'], df.loc[df['flag_check']==2, 'Prot_pred'], marker='^', s=s, color='deepskyblue', linewidths=0.5, edgecolors='black', label='Harmonic flag')

ax.scatter(df.loc[df['flag_check']==3, 'Prot'], df.loc[df['flag_check']==3, 'Prot_pred'], marker='d', s=int(s*1.5), color='crimson', linewidths=0.5, edgecolors='black', label=r'Long trend flag ($P_\mathrm{rot,ML} \geq 38$ days)')

ax.scatter(df.loc[df['flag_check']==1, 'Prot'], df.loc[df['flag_check']==1, 'Prot_pred'], marker='H', s=s, color='yellow', linewidths=0.5, edgecolors='black', label='Filter flag')

ax.scatter(df.loc[df['flag_check']==6, 'Prot'], df.loc[df['flag_check']==6, 'Prot_pred'], marker='P', s=2*s, color='orange', linewidths=0.5, edgecolors='black', label=r'Stars flagged CP/CB1 with $P_\mathrm{rot,ML} \geq 7$ days')

ax.scatter(df.loc[df['flag_check']==5, 'Prot'], df.loc[df['flag_check']==5, 'Prot_pred'], marker='P', s=2*s, color='lime', linewidths=0.5, edgecolors='black', label='Short trend flag ($P_\mathrm{rot,ML} \leq 1.6$ days)')

ax.scatter(df.loc[df['flag_check']==4, 'Prot'], df.loc[df['flag_check']==4, 'Prot_pred'], marker='*', s=2*s, color='indianred', linewidths=0.5, edgecolors='black', label='Short lightcurves ($\leq 4$ quarters)')





#ax.scatter(corrected['Prot'], corrected['Prot_pred'], marker='*', s=60, color='yellow', linewidths=0.5, edgecolors='black', label='Corrected from S19')
#ax.scatter(slight_corrected['Prot'], slight_corrected['Prot_pred'], marker='*', s=30, color='yellow', 
#                                  linewidths=0.5, edgecolors='black', label=None)

ax.legend (loc=2, fontsize=15)
ax.set_xlim (0, 125)
ax.set_ylim (0, 125)

ax.tick_params (axis='both', labelsize=20)


plt.savefig (path.join (abspath, 'data/plot/final_plot.pdf'), format='pdf')
plt.savefig (path.join (abspath, 'data/plot/final_plot.png'), format='png')

#---------------------------------------------

#fig = plt.figure (figsize=(8,8), num=2)
#ax = fig.add_subplot (111)
#ax.set_xlabel (r'$P_\mathrm{rot,S19}$ (days)', fontsize=20)
#ax.set_ylabel (r'$P_\mathrm{rot,ML}$ (days)', fontsize=20)
#ax.fill_between ([0,150], [0,135], [0,165], alpha=0.3, color='blue')
#ax.fill_between ([0,150], [0,165], [0,195], alpha=0.1, color='green')
#ax.fill_between ([0,150], [0,135], [0,105], alpha=0.1, color='green')

#ax.scatter(df.loc[df['cpcb1_flag']==1, 'Prot'], df.loc[df['cpcb1_flag']==1, 'Prot_pred'], marker='.', s=20, color='black', label=None)
