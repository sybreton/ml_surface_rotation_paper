import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from finding_harmonics import find_harmonic_nc 
from os import path

abspath = path.abspath ('..') + '/'

df = pd.read_csv (abspath + 'data/summaries/summary_filter.csv', index_col=0)
ref = pd.read_csv (abspath + 'data/input_samples/kepler_km_sample.csv')
df = df.join (ref, rsuffix = '_ref')
out = df.loc[np.abs(df['Prot_pred']-df['Prot'])>0.1*df['Prot']]
correc_angela = [3837321, 9593950, 10018467, 10614279, 10843431]
slight_correc_angela = [3550041, 3555898, 3634196, 3834448, 4648984, 4908380, 
                        4912212, 5349936, 6626854, 7661316, 8617364, 9074930, 
                        9221129, 9414380, 9414380, 9699942, 10064860, 10676216, 
                        10753707, 11921871, 12006469, 12215093]
corrected = out.loc[np.intersect1d (out.index, correc_angela)]
slight_corrected = out.loc[np.intersect1d (out.index, slight_correc_angela)]

#df = df.loc[np.setdiff1d (df.index, correc_angela+slight_correc_angela)]
#out = out.loc[np.setdiff1d (out.index, correc_angela+slight_correc_angela)]

print ('Total number of stars', df.index.size)
print ('Outliers', out.index.size)
ml_good = np.setdiff1d (df.index, out.index)

#harmonic selection
s = find_harmonic_nc (df, low=1.6, up=2.4)
sout = find_harmonic_nc (out, low=1.6, up=2.4)
thresh = 1
dfh = df.loc[s>thresh]
print ('Visual check to do to spot harmonic problem', s.loc[s>thresh].size)
print ('Harmonic spotted stars', np.intersect1d (s.loc[s>thresh].index, out.index).size)

# 30 % 'large tolerance zone'
thin = df.loc[np.abs(df['Prot']-df['Prot_pred'])<0.1*df['Prot']] 
large = df.loc[np.abs(df['Prot']-df['Prot_pred'])<0.3*df['Prot']] 
tol = large.loc[np.setdiff1d (large.index, thin.index)]

#instrumental modulation
inst = df.loc[(df['Prot_pred']>38)&(df['Prot_pred']<55)]
#inst = inst.loc[(inst['Prot_GWPS_80']>80)&(inst['Prot_ACF_80']>80)&(inst['Prot_CS_80']>80)]
inst = inst.loc[(inst['Prot_GWPS_80']>80)]
flagged = np.intersect1d (inst.index, sout.index).size
redondant = np.intersect1d (inst.index, sout.loc[sout>thresh].index).size
inst = inst.loc[np.setdiff1d (inst.index, s.loc[s>thresh].index)]
print ('Visual check for instrumental modulation', inst.index.size)
print ('Supplementary outliers flagged with instrumental condition', flagged-redondant)

#long trend
ltrend = df.loc[df['Prot_pred']>80]
print ('Long trend to check', ltrend.index.size)
print ('Outliers corrected thanks to long trend to check', np.intersect1d (ltrend.index, out.index).size)
#short trend
strend = df.loc[df['Prot_pred']<1.6]
print ('Short trend to check', strend.index.size)
print ('Outliers corrected thanks to short trend to check', np.intersect1d (strend.index, out.index).size)


print ('Stars in the large tolerance zone', tol.index.size)
print ('Non spotted stars in the large tolerance zone', 
        np.setdiff1d (tol.index, 
        np.concatenate((s.loc[s>thresh].index, inst.index, ltrend.index))).size)


# creating list of all the star we got correctly
a1 = np.copy (df.loc[np.abs (df['Prot']-df['Prot_pred']) < 0.1 * df['Prot']].index)
a2 = np.copy (inst.index)
a3 = np.copy (s.loc[s>thresh].index)
a4 = np.copy (ltrend.index)
a5 = np.copy (corrected.index)
a6 = np.copy (slight_corrected.index)
a7 = np.copy (strend.index)
right = np.concatenate ((a1, a2, a3, a4, a7))
right = np.unique (right)

checks = np.unique (np.concatenate((a2, a3, a4, a7)))
print ('Total number of visual checks to perform', checks.size)
flagged_right = np.setdiff1d (right, ml_good)
print ('Number of flagged stars that will be corrected', flagged_right.size)
print ('Number of stars with correct rotation period after visual checks', ml_good.size+flagged_right.size)
print ('Final accuracy', (ml_good.size+flagged_right.size)/df.index.size * 100)

plt.rcParams['legend.fontsize'] = 10
plt.ion ()

fig = plt.figure (figsize=(8,8))
ax = fig.add_subplot (111)
ax.set_xlabel (r'$P_\mathrm{rot\_S19}$ (days)')
ax.set_ylabel (r'$P_\mathrm{rot\_ML}$ (days)')

ax.fill_between ([0,100], [0,90], [0,110], alpha=0.3, color='blue')
ax.fill_between ([0,100], [0,110], [0,130], alpha=0.1, color='green')
ax.fill_between ([0,100], [0,70], [0,90], alpha=0.1, color='green')
ax.scatter(df['Prot'], df['Prot_pred'], marker='.', s=20, color='black', label=None)
ax.scatter(tol['Prot'], tol['Prot_pred'], marker='o', s=30, color='peru', linewidths=0.5, edgecolors='black', label='Large tolerance zone')
ax.scatter(inst['Prot'], inst['Prot_pred'], marker='d', s=30, color='crimson', linewidths=0.5, edgecolors='black', label='Instrumental modulation flag')
ax.scatter(dfh['Prot'], dfh['Prot_pred'], marker='^', s=20, color='deepskyblue', linewidths=0.5, edgecolors='black', label='Harmonic flag')
ax.scatter(ltrend['Prot'], ltrend['Prot_pred'], marker='P', s=40, color='indianred', linewidths=0.5, edgecolors='black', label='Long trend flag')

ax.scatter(corrected['Prot'], corrected['Prot_pred'], marker='*', s=60, color='yellow', linewidths=0.5, edgecolors='black', label='Corrected from S19')
ax.scatter(slight_corrected['Prot'], slight_corrected['Prot_pred'], marker='*', s=30, color='yellow', 
                                  linewidths=0.5, edgecolors='black', label=None)

ax.legend (loc=2)

plt.savefig (abspath + 'data/plot/final_plot.pdf', format='pdf')
plt.savefig (abspath + 'data/plot/final_plot.png', format='png')
