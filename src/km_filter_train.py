import numpy as np
import pandas as pd
from pushkin.train import train_rf
from pushkin.classify import classify
from os import path
from tqdm import tqdm

n_train = 1
abspath = path.abspath ('..') + '/'
classify_missing = False

# SWITCH TO TRUE TO SAVE THE SUMMARY FRAME AND THE CLASSIFIER
summary = True
save = True
plot = True #will show only the first plot not to overhelm the notebook

#case = 'main'
case = 'aigrain'
case = 'aigrain_reduced'

for ii in tqdm (range (n_train)) :

    if ii > 0 :
        plot = False

    PATH_OUT_CLASSIFIER = abspath + 'data/rf_storage/'
    PATH_SAMPLES = abspath + '/data/input_samples/'
    file_train = 'kepler_km_sample.csv'

    file_flag = 'kepler_km_full_flag.csv'
    PATH_OUT = abspath + 'data/results_training/filter/'
    fileout_result = 'run_' + str(ii+1) + '_km_filter_frame.csv'

    df = pd.read_csv (PATH_SAMPLES+file_train, index_col=0)

    # Remove label columns
    if case!='aigrain' :
        fileout_classifier = 'km_toupie_filter_classifier.joblib'
        input_param = df.columns.values
        input_param = np.setdiff1d (input_param, np.array(['label_rot', 'Prot', 'CS_GAUSS_20', 
                                                         'CS_GAUSS_55', 'CS_GAUSS_80']))
    if case=='aigrain' :
        fileout_classifier = 'aigrain_period_classifier.joblib'
        input_param = np.array (['ACF_ER_SPH_20', 'ACF_ER_SPH_55', 'BAD_Q_FLAG', 'CS_CHIQ_20',
                               'CS_CHIQ_55', 'CS_GAUSS_1_1_20', 'CS_GAUSS_1_1_55',
                               'CS_GAUSS_1_2_20', 'CS_GAUSS_1_2_55', 'CS_GAUSS_1_3_20',
                               'CS_GAUSS_1_3_55', 'CS_GAUSS_1_4_20', 'CS_GAUSS_1_4_55',
                               'CS_GAUSS_1_5_20', 'CS_GAUSS_1_5_55',
                               'CS_GAUSS_2_1_20', 'CS_GAUSS_2_1_55', 'CS_GAUSS_2_2_20',
                               'CS_GAUSS_2_2_55', 'CS_GAUSS_2_3_20', 'CS_GAUSS_2_3_55',
                               'CS_GAUSS_2_4_20', 'CS_GAUSS_2_4_55', 'CS_GAUSS_2_5_20',
                               'CS_GAUSS_2_5_55', 'CS_GAUSS_3_1_20', 'CS_GAUSS_3_1_55',
                               'CS_GAUSS_3_2_20', 'CS_GAUSS_3_2_55', 'CS_GAUSS_3_3_20',
                               'CS_GAUSS_3_3_55', 'CS_GAUSS_3_4_20', 'CS_GAUSS_3_4_55',
                               'CS_GAUSS_3_5_20', 'CS_GAUSS_3_5_55', 'CS_NOISE_20',
                               'CS_NOISE_55', 'CS_N_FIT_20', 'CS_N_FIT_55', 'CS_SPH_ER_20',
                               'CS_SPH_ER_55', 'END_TIME', 'GWPS_CHIQ_20', 'GWPS_CHIQ_55',
                               'GWPS_GAUSS_1_1_20', 'GWPS_GAUSS_1_1_55', 'GWPS_GAUSS_1_2_20',
                               'GWPS_GAUSS_1_2_55', 'GWPS_GAUSS_1_3_20', 'GWPS_GAUSS_1_3_55',
                               'GWPS_GAUSS_1_4_20', 'GWPS_GAUSS_1_4_55', 'GWPS_GAUSS_1_5_20',
                               'GWPS_GAUSS_1_5_55', 'GWPS_GAUSS_2_1_20', 'GWPS_GAUSS_2_1_55',
                               'GWPS_GAUSS_2_2_20', 'GWPS_GAUSS_2_2_55', 'GWPS_GAUSS_2_3_20',
                               'GWPS_GAUSS_2_3_55', 'GWPS_GAUSS_2_4_20', 'GWPS_GAUSS_2_4_55',
                               'GWPS_GAUSS_2_5_20', 'GWPS_GAUSS_2_5_55', 'GWPS_GAUSS_3_1_20',
                               'GWPS_GAUSS_3_1_55', 'GWPS_GAUSS_3_2_20', 'GWPS_GAUSS_3_2_55',
                               'GWPS_GAUSS_3_3_20', 'GWPS_GAUSS_3_3_55', 'GWPS_GAUSS_3_4_20',
                               'GWPS_GAUSS_3_4_55', 'GWPS_GAUSS_3_5_20', 'GWPS_GAUSS_3_5_55',
                               'GWPS_NOISE_20', 'GWPS_NOISE_55', 'GWPS_N_FIT_20', 'GWPS_N_FIT_55',
                               'GWPS_SPH_ER_20', 'GWPS_SPH_ER_55', 'G_ACF_20', 'G_ACF_55',
                               'H_ACF_20', 'H_ACF_55', 'H_CS_20', 'H_CS_55', 'LENGTH', 'N_BAD_Q',
                               'Prot_ACF_20', 'Prot_ACF_55', 'Prot_CS_20', 'Prot_CS_55',
                               'Prot_GWPS_20', 'Prot_GWPS_55', 'START_TIME', 'Sph_ACF_20',
                               'Sph_ACF_55', 'Sph_CS_20', 'Sph_CS_55', 'Sph_GWPS_20',
                               'Sph_GWPS_55', 'label_filter'])
        df = df.loc[df['label_filter']%3!=2]
        df.loc[df['label_filter']>4, 'label_filter'] -= 1 
        df.loc[df['label_filter']>2, 'label_filter'] -= 1 
        ref = pd.read_csv (path.join (PATH_SAMPLES, 'periods_simu_aigrain.csv'), index_col=0)
        df = df.loc[np.setdiff1d (df.index, ref['KID'])]
        
    if case=='aigrain_reduced' :
        fileout_classifier = 'aigrain_period_classifier_reduced.joblib'
        input_param = np.array (['BAD_Q_FLAG', 'CS_CHIQ_20',
                               'CS_CHIQ_55', 'CS_GAUSS_1_1_20', 'CS_GAUSS_1_1_55',
                               'CS_GAUSS_1_2_20', 'CS_GAUSS_1_2_55', 'CS_GAUSS_1_3_20',
                               'CS_GAUSS_1_3_55', 'CS_GAUSS_1_4_20', 'CS_GAUSS_1_4_55',
                               'CS_GAUSS_1_5_20', 'CS_GAUSS_1_5_55',
                               'CS_GAUSS_2_1_20', 'CS_GAUSS_2_1_55', 'CS_GAUSS_2_2_20',
                               'CS_GAUSS_2_2_55', 'CS_GAUSS_2_3_20', 'CS_GAUSS_2_3_55',
                               'CS_GAUSS_2_4_20', 'CS_GAUSS_2_4_55', 'CS_GAUSS_2_5_20',
                               'CS_GAUSS_2_5_55', 'CS_GAUSS_3_1_20', 'CS_GAUSS_3_1_55',
                               'CS_GAUSS_3_2_20', 'CS_GAUSS_3_2_55', 'CS_GAUSS_3_3_20',
                               'CS_GAUSS_3_3_55', 'CS_GAUSS_3_4_20', 'CS_GAUSS_3_4_55',
                               'CS_GAUSS_3_5_20', 'CS_GAUSS_3_5_55', 'CS_NOISE_20',
                               'CS_NOISE_55', 'CS_N_FIT_20', 'CS_N_FIT_55', 'CS_SPH_ER_20',
                               'CS_SPH_ER_55', 'END_TIME', 'GWPS_CHIQ_20', 'GWPS_CHIQ_55',
                               'GWPS_GAUSS_1_1_20', 'GWPS_GAUSS_1_1_55', 'GWPS_GAUSS_1_2_20',
                               'GWPS_GAUSS_1_2_55', 'GWPS_GAUSS_1_3_20', 'GWPS_GAUSS_1_3_55',
                               'GWPS_GAUSS_1_4_20', 'GWPS_GAUSS_1_4_55', 'GWPS_GAUSS_1_5_20',
                               'GWPS_GAUSS_1_5_55', 'GWPS_GAUSS_2_1_20', 'GWPS_GAUSS_2_1_55',
                               'GWPS_GAUSS_2_2_20', 'GWPS_GAUSS_2_2_55', 'GWPS_GAUSS_2_3_20',
                               'GWPS_GAUSS_2_3_55', 'GWPS_GAUSS_2_4_20', 'GWPS_GAUSS_2_4_55',
                               'GWPS_GAUSS_2_5_20', 'GWPS_GAUSS_2_5_55', 'GWPS_GAUSS_3_1_20',
                               'GWPS_GAUSS_3_1_55', 'GWPS_GAUSS_3_2_20', 'GWPS_GAUSS_3_2_55',
                               'GWPS_GAUSS_3_3_20', 'GWPS_GAUSS_3_3_55', 'GWPS_GAUSS_3_4_20',
                               'GWPS_GAUSS_3_4_55', 'GWPS_GAUSS_3_5_20', 'GWPS_GAUSS_3_5_55',
                               'GWPS_NOISE_20', 'GWPS_NOISE_55', 'GWPS_N_FIT_20', 'GWPS_N_FIT_55',
                               'GWPS_SPH_ER_20', 'GWPS_SPH_ER_55', 'G_ACF_20', 'G_ACF_55',
                               'H_ACF_20', 'H_ACF_55', 'H_CS_20', 'H_CS_55', 'LENGTH', 'N_BAD_Q',
                               'Prot_CS_20', 'Prot_CS_55',
                               'Prot_GWPS_20', 'Prot_GWPS_55', 'START_TIME', 
                               'Sph_CS_20', 'Sph_CS_55', 'Sph_GWPS_20',
                               'Sph_GWPS_55', 'label_filter'])
        df = df.loc[df['label_filter']%3!=2]        
        df = df.loc[df['label_filter']>2]
        df.loc[df['label_filter']>4, 'label_filter'] -= 1 
        df.loc[df['label_filter']>2, 'label_filter'] -= 1 
        ref = pd.read_csv (path.join (PATH_SAMPLES, 'periods_simu_aigrain.csv'), index_col=0)
        df = df.loc[np.setdiff1d (df.index, ref['KID'])]

        
    df = df[input_param]

    df = df.replace (to_replace=-1.0, value=np.nan)
    df = df.replace (to_replace=-999, value=np.nan)
    df = df.replace (to_replace='NaN', value=np.nan)
    df = df.replace (to_replace=np.inf, value=np.nan)
    df = df.replace (to_replace=-np.inf, value=np.nan)
    df = df.dropna ()

    df = df.rename (columns={'label_filter':'label'})

    class_weight = 'balanced'
    test_size = 0.25
    clf = train_rf (df, clf_name=PATH_OUT_CLASSIFIER+fileout_classifier, frame_name=PATH_OUT+fileout_result,
            n_estimators=300, criterion='gini', min_samples_split=2, plot=plot,
            class_weight=class_weight, test_size=test_size, summary=summary, save=save, random_state=ii, verbose=1)

    if classify_missing==True : 
        file_missing = 'kepler_km_missing_parameters.csv'
        df_missing = pd.read_csv (PATH_SAMPLES+file_missing, index_col=0)
        df_missing = df_missing.loc[df_missing['label_rot']=='Prot'] 
        input_param = np.setdiff1d (input_param, np.array(['label_filter'])) 
        df_missing = df_missing[input_param]
        df_missing = frame_filler (df_missing)
        print ('Number of elements initially in the frame with missing parameters: ', df_missing.index.size) 
        results = classify (df_missing, clf=clf, summary=False)

