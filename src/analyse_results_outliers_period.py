import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import os
from os import path
import sys 
from astropy.io import fits
import glob

abspath = path.abspath ('..') + '/'
path_out = path.join (abspath, 'data/summaries/')

analyse_filter=False
os.chdir (path.join (abspath, 'data/results_training/outliers/'))

list_results = glob.glob ('run*.csv')
df = pd.DataFrame ()

for elt in list_results :
  df_aux = pd.read_csv (elt, index_col=0)
  df = pd.concat ([df, df_aux])

dfw = df.loc[df['label']!=df['label_pred']]
summary = df.groupby (df.index).mean()
fileref = path.join (abspath, 'data/input_samples/kepler_km_sample.csv')
ref = pd.read_csv (fileref, index_col=0)

count = dfw.index.value_counts().to_frame ()
count = count.rename (columns={0:'misclassified'})
count = count.join (df.index.value_counts().to_frame())
count = count.rename (columns={0:'drawn'})
count['ratio'] = count['misclassified'] / count['drawn'] 
count = count.join(summary['label'])

summary['label_pred'] = 0
summary.loc[summary['1']>=0.5,'label_pred'] = 1

summary.to_csv (path.join (path_out, 'summary_outliers_period.csv'))

print ('Number of stars in the sample', summary.index.size)
print ('Number of stars misclassified at least once:', count.loc[count['ratio']>0].index.size)
print ('Number of stars misclassified half of the time:', count.loc[count['ratio']>0.5].index.size)
print ('Number of stars always misclassified:', count.loc[count['ratio']==1].index.size)

isOut = (count['label'] == 1)
print ('Number of outliers misclassified at least once:', count.loc[(count['ratio']>0)&isOut].index.size)
print ('Number of outliers misclassified half of the time:', count.loc[(count['ratio']>0.5)&isOut].index.size)
print ('Number of outliers always misclassified:', count.loc[(count['ratio']==1)&isOut].index.size)

print ('Number of outliers:',  summary.loc[summary['label']==1].index.size)
print ('Number of outliers correctly classified with the global score:', summary.loc[(summary['label_pred']==1)&(summary['label']==1)].index.size)
print ('Number of outliers misclassified with the global score:', summary.loc[(summary['label_pred']==0)&(summary['label']==1)].index.size)

print ('Fraction of non-outliers misclassified:',  
       summary.loc[(summary['label']==0)&(summary['label_pred']==1)].index.size / summary.loc[summary['label']==0].index.size)

print ('Fraction of the sample to visually check:',  
       summary.loc[(summary['label_pred']==1)].index.size / summary.index.size)


print ('Accuracy:', summary.loc[summary['label_pred']==summary['label']].index.size / summary.index.size)


os.chdir (path.join (abspath,'src/'))

