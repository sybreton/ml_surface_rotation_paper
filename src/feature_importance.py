import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from joblib import load
from os import path

upDir = path.abspath ('..')
srcDir = path.abspath ('.')
rfDir = path.abspath ('../data/rf_storage')
csvDir = path.abspath ('../data/input_samples')

file_rot = path.join (rfDir, 'km_toupie_rot_classifier.joblib')
file_cpcb = path.join (rfDir, 'km_toupie_cpcb_classifier.joblib')
file_filter = path.join (rfDir, 'km_toupie_filter_classifier.joblib')

df = pd.read_csv (path.join (csvDir, 'kepler_km_sample.csv'), index_col=0)
input_param = df.columns.values
input_param = np.setdiff1d (input_param, np.array(['label_filter', 'label_rot', 
                                                 'Prot', 'CS_GAUSS_20', 
                                                 'CS_GAUSS_55', 'CS_GAUSS_80']))
df = df[input_param]

clf_rot = load (file_rot)
clf_cpcb = load (file_cpcb)
clf_filter = load (file_filter)

feat_rot = pd.DataFrame (index=df.columns, data=clf_rot.feature_importances_)
feat_cpcb = pd.DataFrame (index=df.columns, data=clf_cpcb.feature_importances_)
feat_filter = pd.DataFrame (index=df.columns, data=clf_filter.feature_importances_)


fig = plt.figure (figsize=(30,10))
ax1 = fig.add_subplot (131)
ax2 = fig.add_subplot (132)
ax3 = fig.add_subplot (133)

n = 10
y = [i for i in range (1, n+1)]
ax1.barh (y, feat_rot.nlargest (n, 0)[0], color='orange')
ax2.barh (y, feat_cpcb.nlargest (n, 0)[0], color='orange')
ax3.barh (y, feat_filter.nlargest (n, 0)[0], color='orange')

ax1.set_yticks (y)
ax2.set_yticks (y)
ax3.set_yticks (y)

ax1.set_yticklabels (feat_rot.nlargest (n, 0).index, color='black')
ax2.set_yticklabels (feat_cpcb.nlargest (n, 0).index, color='black')
ax3.set_yticklabels (feat_filter.nlargest (n, 0).index, color='black')

ax1.tick_params (axis='x', labelsize=20)
ax2.tick_params (axis='x', labelsize=20)
ax3.tick_params (axis='x', labelsize=20)

pad = -600.

ax2.set_xlabel (r'Relative importance', fontsize=30)
ax1.set_ylabel (r'Parameter', fontsize=30)

ax1.tick_params (axis='y', labelleft=False, labelright=True, left=False, labelsize=22, pad=pad)
ax2.tick_params (axis='y', labelleft=False, labelright=True, left=False, labelsize=22, pad=pad)
ax3.tick_params (axis='y', labelleft=False, labelright=True, left=False, labelsize=22, pad=pad)

pos = 0.95
ax1.text (pos, pos, 'RotClass', horizontalalignment='right', verticalalignment='top', fontsize=30,
         transform=ax1.transAxes)
ax2.text (pos, pos, 'PollClass', horizontalalignment='right', verticalalignment='top', fontsize=30,
         transform=ax2.transAxes)
ax3.text (pos, pos, 'PeriodSel', horizontalalignment='right', verticalalignment='top', fontsize=30,
         transform=ax3.transAxes)

plt.subplots_adjust(left=0.05, bottom=0.1, right=0.95, top=0.95, wspace=0.1, hspace=0.05)

plt.savefig (path.join (upDir, 'data/plot/feature_importance.pdf'), format='pdf')

plt.show ()












