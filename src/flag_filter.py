import numpy as np
import pandas as pd

def flag_filter (df, thresh=0.15, change_sph_and_error=False, only_gwps=False) :

    values = ['Prot_ACF_20', 'Prot_ACF_55', 'Prot_ACF_80',
       'Prot_CS_20', 'Prot_CS_55', 'Prot_CS_80', 'Prot_GWPS_20',
       'Prot_GWPS_55', 'Prot_GWPS_80']
    
    errors = np.array ([
                       'CS_GAUSS_3_1_20', 'CS_GAUSS_3_1_55', 'CS_GAUSS_3_1_80',
                       'GWPS_GAUSS_3_1_20', 'GWPS_GAUSS_3_1_55', 'GWPS_GAUSS_3_1_80'])
    
    sph = np.array (['Sph_ACF_20', 'Sph_ACF_55', 'Sph_ACF_80', 
                     'Sph_CS_20', 'Sph_CS_55', 'Sph_CS_80', 
                     'Sph_GWPS_20', 'Sph_GWPS_55', 'Sph_GWPS_80'])
    
    sph_error = (['ACF_ER_SPH_20', 'ACF_ER_SPH_55', 'ACF_ER_SPH_80',
                 'CS_SPH_ER_20', 'CS_SPH_ER_55', 'CS_SPH_ER_80',
                 'GWPS_SPH_ER_20', 'GWPS_SPH_ER_55', 'GWPS_SPH_ER_80'])
    
    if only_gwps :
        period_20 = df[['Prot_GWPS_20']].to_numpy ()
        period_55 = df[['Prot_GWPS_55']].to_numpy ()
        period_80 = df[['Prot_GWPS_80']].to_numpy ()

        err_20 = df[['GWPS_GAUSS_3_1_20',]].to_numpy ()
        err_55 = df[['GWPS_GAUSS_3_1_55',]].to_numpy ()
        err_80 = df[['GWPS_GAUSS_3_1_80',]].to_numpy ()

        sph_20 = df[['Sph_GWPS_20']].to_numpy ()
        sph_55 = df[['Sph_GWPS_55']].to_numpy ()
        sph_80 = df[['Sph_GWPS_80']].to_numpy ()

        sph_err_20 = df[['GWPS_SPH_ER_20']].to_numpy ()
        sph_err_55 = df[['GWPS_SPH_ER_20']].to_numpy ()
        sph_err_80 = df[['GWPS_SPH_ER_20']].to_numpy ()
        
    else :
        period_20 = df[['Prot_ACF_20', 'Prot_CS_20', 'Prot_GWPS_20']].to_numpy ()
        period_55 = df[['Prot_ACF_55', 'Prot_CS_55', 'Prot_GWPS_55']].to_numpy ()
        period_80 = df[['Prot_ACF_80', 'Prot_CS_80', 'Prot_GWPS_80']].to_numpy ()

        err_20 = df[['CS_GAUSS_3_1_20', 'GWPS_GAUSS_3_1_20',]].to_numpy ()
        err_20 = np.c_[np.full(err_20.shape[0], -1), err_20]
        err_55 = df[['CS_GAUSS_3_1_55', 'GWPS_GAUSS_3_1_55',]].to_numpy ()
        err_55 = np.c_[np.full(err_55.shape[0], -1), err_55]
        err_80 = df[['CS_GAUSS_3_1_80', 'GWPS_GAUSS_3_1_80',]].to_numpy ()
        err_80 = np.c_[np.full(err_80.shape[0], -1), err_80]

        sph_20 = df[['Sph_ACF_20', 'Sph_CS_20', 'Sph_GWPS_20']].to_numpy ()
        sph_55 = df[['Sph_ACF_55', 'Sph_CS_55', 'Sph_GWPS_55']].to_numpy ()
        sph_80 = df[['Sph_ACF_80', 'Sph_CS_80', 'Sph_GWPS_80']].to_numpy ()

        sph_err_20 = df[['ACF_ER_SPH_20', 'CS_SPH_ER_20', 'GWPS_SPH_ER_20']].to_numpy ()
        sph_err_55 = df[['ACF_ER_SPH_20', 'CS_SPH_ER_20', 'GWPS_SPH_ER_20']].to_numpy ()
        sph_err_80 = df[['ACF_ER_SPH_20', 'CS_SPH_ER_20', 'GWPS_SPH_ER_20']].to_numpy ()
    
    prot = df['Prot_pred'].to_numpy ()

    ratio_20 = np.empty (period_20.shape)
    ratio_55 = np.empty (period_55.shape)
    ratio_80 = np.empty (period_80.shape)

    closest_20 = np.empty (period_20.shape[0])
    closest_55 = np.empty (period_55.shape[0])
    closest_80 = np.empty (period_80.shape[0])
    err_closest_20 = np.empty (period_20.shape[0])
    err_closest_55 = np.empty (period_55.shape[0])
    err_closest_80 = np.empty (period_80.shape[0])
    sph_closest_20 = np.empty (period_20.shape[0])
    sph_closest_55 = np.empty (period_55.shape[0])
    sph_closest_80 = np.empty (period_80.shape[0])
    sph_err_closest_20 = np.empty (period_20.shape[0])
    sph_err_closest_55 = np.empty (period_55.shape[0])
    sph_err_closest_80 = np.empty (period_80.shape[0])

    for ii in range (period_20.shape[1]) :
        ratio_20[:,ii] = np.abs (period_20[:,ii] - prot) / prot
        ratio_55[:,ii] = np.abs (period_55[:,ii] - prot) / prot
        ratio_80[:,ii] = np.abs (period_80[:,ii] - prot) / prot

    for ii, icol in enumerate (np.argmin(ratio_20, axis=1)) :   
        closest_20[ii] = period_20[ii,icol]
        err_closest_20[ii] = err_20[ii, icol]
        sph_closest_20[ii] = sph_20[ii,icol]
        sph_err_closest_20[ii] = sph_err_20[ii,icol]
    for ii, icol in enumerate (np.argmin(ratio_55, axis=1)) :   
        closest_55[ii] = period_55[ii,icol]
        err_closest_55[ii] = err_55[ii, icol]
        sph_closest_55[ii] = sph_55[ii,icol]
        sph_err_closest_55[ii] = sph_err_55[ii,icol]
    for ii, icol in enumerate (np.argmin(ratio_55, axis=1)) :   
        closest_80[ii] = period_80[ii,icol]
        err_closest_80[ii] = err_80[ii, icol]
        sph_closest_80[ii] = sph_80[ii,icol]
        sph_err_closest_80[ii] = sph_err_80[ii,icol]


    cond_20da = (df['label_pred']%3!=0)&(df['Prot_pred']<23)
    cond_20db = np.any (ratio_20 < thresh, axis=1)
    cond_55da = (df['label_pred']%3!=1)&((df['Prot_pred']<60)&(df['Prot_pred']>23)) 
    cond_55db = np.any (ratio_55 < thresh, axis=1)
    cond_80da = (df['label_pred']%3!=2)&(df['Prot_pred']>60) 
    cond_80db = np.any (ratio_80 < thresh, axis=1)

    # Correcting Prot when in the good filter we have a period within a threshold 
    # with the ML predicted period
    n = df.loc[cond_20da&cond_20db].index.size + df.loc[cond_80da&cond_80db].index.size + df.loc[cond_55da&cond_55db].index.size
    print ('Number of stars for which filter condition will change Prot:', n)
    
    df.loc[cond_20da&cond_20db, 'Prot_pred'] = closest_20[cond_20da&cond_20db]
    df.loc[cond_55da&cond_55db, 'Prot_pred'] = closest_55[cond_55da&cond_55db]
    df.loc[cond_80da&cond_80db, 'Prot_pred'] = closest_80[cond_80da&cond_80db]

    df.loc[cond_20da&cond_20db, 'flag_check'] = -1
    df.loc[cond_55da&cond_55db, 'flag_check'] = -1
    df.loc[cond_80da&cond_80db, 'flag_check'] = -1

    # Flagging wrong filters without good periods
    df.loc[cond_20da&(~cond_20db), 'flag_check'] = 1
    df.loc[cond_55da&(~cond_55db), 'flag_check'] = 1
    df.loc[cond_80da&(~cond_80db), 'flag_check'] = 1

    #print (df.loc[df['flag_check']==-1].index.size)
    #print (df.loc[df['flag_check']==1].index.size)
    
    if change_sph_and_error :
            df.loc[cond_20da&cond_20db, 'Prot_error'] = err_closest_20[cond_20da&cond_20db]
            df.loc[cond_55da&cond_55db, 'Prot_error'] = err_closest_55[cond_55da&cond_55db]
            df.loc[cond_80da&cond_80db, 'Prot_error'] = err_closest_80[cond_80da&cond_80db]
            
            df.loc[cond_20da&cond_20db, 'Sph_pred'] = sph_closest_20[cond_20da&cond_20db]
            df.loc[cond_55da&cond_55db, 'Sph_pred'] = sph_closest_55[cond_55da&cond_55db]
            df.loc[cond_80da&cond_80db, 'Sph_pred'] = sph_closest_80[cond_80da&cond_80db]
            
            df.loc[cond_20da&cond_20db, 'Sph_error'] = sph_err_closest_20[cond_20da&cond_20db]
            df.loc[cond_55da&cond_55db, 'Sph_error'] = sph_err_closest_55[cond_55da&cond_55db]
            df.loc[cond_80da&cond_80db, 'Sph_error'] = sph_err_closest_80[cond_80da&cond_80db]        
    
    return df
