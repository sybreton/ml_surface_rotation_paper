import pandas as pd
import numpy as np

def find_harmonic (df, low=1.9, up=2.1) :

  cond1 = (df['Prot_GWPS_55'] / df['Prot_pred']> low) & (df['Prot_GWPS_55'] / df['Prot_pred']< up)  
  cond2 = (df['Prot_ACF_55']  / df['Prot_pred']> low) & (df['Prot_ACF_55']  / df['Prot_pred']< up) 
  cond3 = (df['Prot_CS_55']   / df['Prot_pred']> low) & (df['Prot_CS_55']   / df['Prot_pred']< up)  
  cond4 = (df['Prot_GWPS_80'] / df['Prot_pred']> low) & (df['Prot_GWPS_80'] / df['Prot_pred']< up)  
  cond5 = (df['Prot_ACF_80']  / df['Prot_pred']> low) & (df['Prot_ACF_80']  / df['Prot_pred']< up)  
  cond6 = (df['Prot_CS_80']   / df['Prot_pred']> low) & (df['Prot_CS_80']   / df['Prot_pred']< up)  

  control2 = (df['H_ACF_55']>0.3) & (df['G_ACF_55']>0.2)
  control3 = (df['H_CS_55']>0.3)
  control5 = (df['H_ACF_80']>0.3) & (df['G_ACF_80']>0.2)
  control6 = (df['H_CS_80']>0.3)

  l = [cond1, cond2 & control2, cond3 & control3, cond4, cond5 & control5, cond6 & control6]


  s = pd.Series (data=np.zeros(df.index.size), index=df.index)
  for cond in l :
    s.loc[cond] += 1

  return s

def find_harmonic_nc (df, low=1.7, up=2.4, use_all_filter=False) :

    cond1 = (df['Prot_GWPS_55'] / df['Prot_pred']> low) & (df['Prot_GWPS_55'] / df['Prot_pred']< up)  
    cond2 = (df['Prot_ACF_55']  / df['Prot_pred']> low) & (df['Prot_ACF_55']  / df['Prot_pred']< up) 
    cond3 = (df['Prot_CS_55']   / df['Prot_pred']> low) & (df['Prot_CS_55']   / df['Prot_pred']< up)  
    cond4 = (df['Prot_GWPS_80'] / df['Prot_pred']> low) & (df['Prot_GWPS_80'] / df['Prot_pred']< up)  
    cond5 = (df['Prot_ACF_80']  / df['Prot_pred']> low) & (df['Prot_ACF_80']  / df['Prot_pred']< up)  
    cond6 = (df['Prot_CS_80']   / df['Prot_pred']> low) & (df['Prot_CS_80']   / df['Prot_pred']< up)  
    
    cond7 = (df['Prot_GWPS_20'] / df['Prot_pred']> low) & (df['Prot_GWPS_20'] / df['Prot_pred']< up)  
    cond8 = (df['Prot_ACF_20']  / df['Prot_pred']> low) & (df['Prot_ACF_20']  / df['Prot_pred']< up) 
    cond9 = (df['Prot_CS_20']   / df['Prot_pred']> low) & (df['Prot_CS_20']   / df['Prot_pred']< up)  

    if use_all_filter :
        l = [cond1, cond2, cond3, cond4, cond5, cond6, cond7, cond8, cond9]
    else :
        l = [cond1, cond2, cond3, cond4, cond5, cond6]

    s = pd.Series (data=np.zeros(df.index.size), index=df.index)
    for cond in l :
        s.loc[cond] += 1

    return s

