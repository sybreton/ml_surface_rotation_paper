from os import path
import numpy as np
import pandas as pd
from pushkin.classify import classify

def unleash_rooster_on_simulations (clf_type, filename='table_aigrain_with_80d.csv') :

    mainDir = path.abspath ('../../star_rotation')
    clfDir = path.join (path.abspath ('../data'), 'rf_storage')

    if clf_type=='full' :
        rot_clf = path.join (clfDir, 'aigrain_80_rot_classifier.joblib')
        filter_clf = path.join (clfDir, 'aigrain_80_period_classifier.joblib')
        #cpcb_clf = path.join (clfDir, 'new_cpcb_classifier.joblib')
    if clf_type=='reduced' :
        rot_clf = path.join (clfDir, 'aigrain_80_rot_classifier_reduced.joblib')
        filter_clf = path.join (clfDir, 'aigrain_80_period_classifier_reduced.joblib')
        #cpcb_clf = path.join (clfDir, 'reduced_cpcb_classifier.joblib')


    df_in = pd.read_csv (path.join(mainDir, 'data/rotation_tables', filename), index_col=0)

    input_param = df_in.columns.values

    if clf_type=='full' :
        input_param = np.setdiff1d (input_param, np.array(['CS_GAUSS_20', 'CS_GAUSS_55', 'CS_GAUSS_80']))
    if clf_type=='reduced' :
        input_param = np.setdiff1d (input_param, np.array(['CS_GAUSS_20', 'CS_GAUSS_55', 'CS_GAUSS_80', 'Sph_ACF_80', 'Sph_ACF_55',
                                                         'Prot_ACF_80', 'Sph_ACF_20', 'Prot_ACF_20', 'Prot_ACF_55',
                                                          'ACF_ER_SPH_20', 'ACF_ER_SPH_55', 'ACF_ER_SPH_80',]))
    df_in = df_in[input_param]

    #df_in = df_in.replace (to_replace=-1.0, value=np.nan)
    #df_in = df_in.replace (to_replace=-999.0, value=np.nan)
    df_in = df_in.replace (to_replace=np.inf, value=np.nan)
    df_in = df_in.replace (to_replace=-np.inf, value=np.nan)
    df_in = df_in.replace (to_replace='NaN', value=np.nan)
    df_in = df_in.dropna ()

    df_rot = classify (df_in, clf_name=rot_clf, summary=False)
    df_filter = classify (df_in, clf_name=filter_clf, summary=False)
    #df_cpcb = classify (df_in, clf_name=cpcb_clf, summary=False)


    #attributing rotation period
    df_in = pd.read_csv (path.join(mainDir, 'data/rotation_tables', filename), index_col=0)
    represented_class = np.sort (df_filter['label_pred'].unique ().astype (np.float_).astype (np.int_))
    values = np.array (['Prot_ACF_20', 'Prot_ACF_55', 'Prot_ACF_80',
             'Prot_CS_20', 'Prot_CS_55', 'Prot_CS_80', 'Prot_GWPS_20',
             'Prot_GWPS_55', 'Prot_GWPS_80'])

    errors = np.array (['CS_GAUSS_3_1_20', 'CS_GAUSS_3_1_55', 'CS_GAUSS_3_1_80',
                        'GWPS_GAUSS_3_1_20', 'GWPS_GAUSS_3_1_55', 'GWPS_GAUSS_3_1_80'])

    sph = np.array (['Sph_ACF_20', 'Sph_ACF_55', 'Sph_ACF_80',
                     'Sph_CS_20', 'Sph_CS_55', 'Sph_CS_80',
                     'Sph_GWPS_20', 'Sph_GWPS_55', 'Sph_GWPS_80'])

    sph_error = (['ACF_ER_SPH_20', 'ACF_ER_SPH_55', 'ACF_ER_SPH_80',
                 'CS_SPH_ER_20', 'CS_SPH_ER_55', 'CS_SPH_ER_80',
                 'GWPS_SPH_ER_20', 'GWPS_SPH_ER_55', 'GWPS_SPH_ER_80'])
    
    acf_err_20 = np.full (df_filter.index.size, -1)
    acf_err_55 = np.full (df_filter.index.size, -1)
    acf_err_80 = np.full (df_filter.index.size, -1)

    acf_err = np.c_[acf_err_20, acf_err_55, acf_err_80]
    
    pos_prot = df_in.loc[df_filter.index, values].to_numpy ()
    prot_err = df_in.loc[df_filter.index, errors].to_numpy ()
    prot_err = np.c_[acf_err, prot_err]
    pos_sph = df_in.loc[df_filter.index, sph].to_numpy ()
    sph_err = df_in.loc[df_filter.index, sph_error].to_numpy ()
    
    index = df_filter['label_pred'].to_numpy ()
    index = index.astype(np.float_).astype (np.int_)
    prot_pred = np.full (df_filter.index.size, -9999.)
    prot_err_pred = np.full (df_filter.index.size, -9999.)
    sph_pred = np.full (df_filter.index.size, -9999.)
    sph_err_pred = np.full (df_filter.index.size, -9999.)

    
    for ii, indprot in enumerate (index) :
        prot_pred[ii] = pos_prot[ii, indprot]
        prot_err_pred[ii] = prot_err[ii, indprot]
        sph_pred[ii] = pos_sph[ii, indprot]
        sph_err_pred[ii] = sph_err[ii, indprot]

    df_filter['Prot_pred'] = prot_pred
    df_filter['Prot_error'] = prot_err_pred
    df_filter['Sph_pred'] = sph_pred
    df_filter['Sph_error'] = sph_err_pred
    
    df_rot = df_rot.add_suffix ('_rot')
    df_filter = df_filter.add_suffix ('_period')
    #df_cpcb = df_cpcb.add_suffix ('_cpcb')

    
    df_glob = df_rot.join (df_filter)
    #df_glob = df_glob.join (df_cpcb)
    
    df_glob = df_glob.rename (columns={'Prot_pred_period':'Prot_pred',
                                      'Prot_error_period':'Prot_error',
                                      'Sph_pred_period':'Sph_pred',
                                      'Sph_error_period':'Sph_error'})

    
    return df_glob
                             
if __name__ == '__main__' :
                             
    df_full = unleash_rooster_on_simulations ('full', filename='table_aigrain_with_80d.csv')
    df_reduced = unleash_rooster_on_simulations ('reduced', filename='table_aigrain_with_80d.csv')
    aux = np.setdiff1d (df_reduced.index, df_full.index)
    df = pd.concat ([df_full, df_reduced.loc[aux]])
    df['flag_reduced'] = 0
    df.loc[aux, 'flag_reduced'] = 1