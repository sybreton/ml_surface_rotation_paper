import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import os
from os import path
import sys 
from astropy.io import fits
import glob

abspath = path.abspath ('..')
path_out = path.join (abspath, 'data/summaries/')

os.chdir (path.join (abspath, 'data/results_training/filter/'))

list_results = glob.glob ('run*.csv')
df = pd.DataFrame ()

for elt in list_results :
  df_aux = pd.read_csv (elt, index_col=0)
  df = pd.concat ([df, df_aux])

summary = df.groupby (df.index).mean()
fileref = path.join (abspath, 'data/input_samples/kepler_km_sample.csv') 
ref = pd.read_csv (fileref, index_col=0)


#TODO
aux = df[np.intersect1d(df.columns, ['0.0', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '8.0'])].to_numpy ()
represented_class = np.sort (summary['label'].unique ().astype (np.int_))
values = np.array (['Prot_ACF_20', 'Prot_ACF_55', 'Prot_ACF_80',
         'Prot_CS_20', 'Prot_CS_55', 'Prot_CS_80', 'Prot_GWPS_20',
         'Prot_GWPS_55', 'Prot_GWPS_80'])
values = values[represented_class]
df['label_pred'] = np.argmax (aux, axis=1)
df['label'] = df['label'].astype (np.int_)
df = df.join (ref[values])
df = df.join (ref['Prot'])
pos_prot = df[values].to_numpy ()
index = df['label_pred'].to_numpy ()
prot_pred = np.full (df.index.size, -9999.)
for ii, indprot in enumerate (index) :
    prot_pred[ii] = pos_prot[ii, indprot]
prot = df['Prot'].to_numpy()
diff = np.abs (prot_pred-prot)
df['Prot_pred'] = prot_pred
df['diff'] = diff
cond = diff > 0.1 * prot

dfw = df.loc[cond]
count = dfw.index.value_counts().to_frame ()
count = count.rename (columns={0:'misclassified'})
count = count.join (df.index.value_counts().to_frame())
count = count.rename (columns={0:'drawn'})
count['ratio'] = count['misclassified'] / count['drawn'] 

print ('Number of stars in the sample', summary.index.size)
print ('Number of stars with wrong Prot at least once:', count.loc[count['ratio']>0].index.size)
print ('Number of stars with wrong Prot half of the time:', count.loc[count['ratio']>0.5].index.size)
print ('Number of stars always with wrong Prot:', count.loc[count['ratio']==1].index.size)

print ('Global accuracy: Number of stars correctly labelled', summary[summary['label_pred']==summary['label']].index.size)
print ('Corresponding fraction', summary[summary['label_pred']==summary['label']].index.size /summary.index.size)


aux = summary[np.intersect1d(summary.columns, ['0.0', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '8.0'])].to_numpy ()

summary = summary.join (ref['Prot'])
summary = summary.join (ref[values])
pos_prot = summary[values].to_numpy ()
summary['label_pred'] = np.argmax (aux, axis=1)
summary['label'] = summary['label'].astype (np.int_)

index = summary['label_pred'].to_numpy ()
prot_pred = np.full (summary.index.size, -9999.)
for ii, indprot in enumerate (index) :
    prot_pred[ii] = pos_prot[ii, indprot]
prot = summary['Prot'].to_numpy()
diff = np.abs (prot_pred-prot)
summary['Prot_pred'] = prot_pred
summary['diff'] = diff
cond = diff > 0.1 * prot

outliers = np.count_nonzero (cond)
within = summary.index[~cond]
print ('Number of stars for which ML Prot is outside 10% from reference value',
          outliers)
print ('Number of period retrieved within 10%:', within.size )
print ('Percentage of period retrieved within 10%', 100 - outliers / summary.index.size * 100)


    
summary.to_csv (path.join (path_out, 'summary_filter.csv'))

os.chdir (path.join (abspath,'src/'))

