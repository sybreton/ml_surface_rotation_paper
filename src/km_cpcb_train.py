import numpy as np
import pandas as pd
from pushkin.train import train_rf
from pushkin.classify import classify
from os import path
from tqdm import tqdm

n_train = 100
abspath = path.abspath ('..') + '/'
classify_missing = False

# SWITCH TO TRUE TO SAVE THE SUMMARY FRAME AND THE CLASSIFIER
summary = True
save = True
plot = True #will show only the first plot not to overhelm the notebook

for ii in tqdm (range (n_train)) :

  if ii > 0 :
    plot = False

  PATH_OUT_CLASSIFIER = abspath + 'data/rf_storage/'
  PATH_SAMPLES = abspath + 'data/input_samples/'
  file_train = 'kepler_km_sample.csv'
  fileout_classifier = 'km_toupie_cpcb_classifier.joblib'
  file_flag = 'kepler_km_full_flag.csv'
  PATH_OUT = abspath + 'data/results_training/cpcb/'
  fileout_result = 'run_' + str(ii+1) + '_km_cpcb_frame.csv'

  df = pd.read_csv (PATH_SAMPLES+file_train, index_col=0)
  df = df.loc[df['label_rot']=='Prot']

  # Remove label columns
  input_param = df.columns.values
  input_param = np.setdiff1d (input_param, np.array(['label_filter', 'label_rot', 
                                                     'Prot', 'CS_GAUSS_20', 
                                                     'CS_GAUSS_55', 'CS_GAUSS_80']))
  df = df[input_param]

  df = df.replace (to_replace=-1.0, value=np.nan)
  df = df.replace (to_replace=-999, value=np.nan)
  df = df.replace (to_replace='NaN', value=np.nan)
  df = df.replace (to_replace=np.inf, value=np.nan)
  df = df.replace (to_replace=-np.inf, value=np.nan)
  df = df.dropna ()

  flag = pd.read_csv (PATH_SAMPLES+file_flag, index_col=0)

  #FLAG CPCB
  cpcb = flag.loc[flag['cpcb1_flag']==1].index
  mssl = flag.loc[flag['cpcb1_flag']==-999].index
  df_mssl = df.loc[np.intersect1d(df.index, mssl)].sample (n=cpcb.size)
  df_cpcb = df.loc[np.intersect1d(df.index, cpcb)]
  
  df_mssl['label'] = '0'
  df_cpcb['label'] = '1'
  df = pd.concat([df_mssl, df_cpcb])

  # REMOVE KOI
  koi = flag.loc[flag['koi_flag']!=-999].index
  #df = df.loc[np.setdiff1d(df.index, koi)]
  #PATH_OUT = PATH_OUT+'noKOI/'
  
  class_weight = 'balanced'
  test_size = 0.25

  clf = train_rf (df, clf_name=PATH_OUT_CLASSIFIER+fileout_classifier, frame_name=PATH_OUT+fileout_result,
            n_estimators=300, criterion='gini', min_samples_split=2, plot=plot,
            class_weight=class_weight, test_size=test_size, summary=summary, save=save, random_state=ii, verbose=1)

  if classify_missing==True : 
    file_missing = 'kepler_km_missing_parameters.csv'
    df_missing = pd.read_csv (PATH_SAMPLES+file_missing, index_col=0)
    df_missing = df_missing.loc[df_missing['label_rot']=='Prot'] 
    input_param = np.setdiff1d (input_param, np.array(['label_filter'])) 
    df_missing = df_missing[input_param]
    df_missing = frame_filler (df_missing)
    print ('Number of elements initially in the frame with missing parameters: ', df_missing.index.size) 
    results = classify (df_missing, clf=clf, summary=False)

