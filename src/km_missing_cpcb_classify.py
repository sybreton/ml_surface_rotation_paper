import numpy as np
import pandas as pd
from pushkin.train import train_rf
from pushkin.classify import classify
from os import path
import sys
from tqdm import tqdm

n_train = 1
abspath = path.abspath ('..') + '/'
classify_missing = False

# SWITCH TO TRUE TO SAVE THE SUMMARY FRAME AND THE CLASSIFIER
summary = False
save = False
plot = True #will show only the first plot not to overhelm the notebook

PATH_OUT_CLASSIFIER = abspath + 'data/rf_storage/'
PATH_OUT = abspath + 'data/results_training/rotation/'
PATH_SAMPLES = abspath + '/data/input_samples/'
file_train = 'kepler_km_sample.csv'
file_flag = 'kepler_km_full_flag.csv'
flag = pd.read_csv (PATH_SAMPLES+file_flag, index_col=0)


file_missing = 'missing_sample.csv'
df_missing = pd.read_csv (PATH_SAMPLES+file_missing, index_col=0)


df = pd.read_csv (PATH_SAMPLES+file_train, index_col=0)
df = df.loc[np.setdiff1d (df.index, df_missing.index)]

cpcb = flag.loc[flag['cpcb1_flag']==1].index
mssl = flag.loc[flag['cpcb1_flag']==-999].index
df_mssl = df.loc[np.intersect1d(df.index, mssl)].sample (n=cpcb.size)
df_cpcb = df.loc[np.intersect1d(df.index, cpcb)]
  
df_mssl['label'] = '0'
df_cpcb['label'] = '1'
df = pd.concat([df_mssl, df_cpcb])




# Remove label columns
input_param = df.columns.values
input_param = np.setdiff1d (input_param, np.array(['Prot', 'CS_GAUSS_20', 'CS_GAUSS_55', 'CS_GAUSS_80', 'Sph_ACF_80', 'Sph_ACF_55',
                                                     'Prot_ACF_80', 'Sph_ACF_20', 'Prot_ACF_20', 'Prot_ACF_55',
                                                     'ACF_ER_SPH_20', 'ACF_ER_SPH_55', 'ACF_ER_SPH_80',
                                                     'label_filter', 'label_rot']))
df = df[input_param]

# taking care of missing values
df = df.replace (to_replace=-1.0, value=np.nan)
df = df.replace (to_replace=-999.0, value=np.nan)
df = df.replace (to_replace=np.inf, value=np.nan)
df = df.replace (to_replace=-np.inf, value=np.nan)
df = df.replace (to_replace='NaN', value=np.nan)
df = df.dropna ()
print (df.index.size)

class_weight = 'balanced'
test_size = 0.01
  
for ii in tqdm (range (n_train)) :
    
    if ii > 0 :
        plot = False
    
    clf = train_rf (df, clf_name=None, frame_name=None,
            n_estimators=300, criterion='gini', min_samples_split=2, plot=plot,
            class_weight=class_weight, test_size=test_size, summary=summary, save=save,
            random_state=ii, verbose=1)

ref = pd.read_csv (PATH_SAMPLES+file_train, index_col=0)    
input_param = np.setdiff1d (input_param, np.array(['label'])) 
df_missing = df_missing.loc[np.intersect1d(df_missing.index, ref.loc[~ref['Prot'].isna()].index)]
df_missing = df_missing[input_param]
results = classify (df_missing, clf=clf, summary=False)

results = results.join (flag['cpcb1_flag'])
results = results.rename (columns={'cpcb1_flag':'label'})

results.loc[results['label']==-999, 'label'] = 0

results.to_csv (abspath+'data/summaries/missing_cpcb.csv')



