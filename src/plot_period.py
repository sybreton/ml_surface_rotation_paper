import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from os import path
from scipy.stats import gaussian_kde

abspath = path.abspath ('..') + '/'

df = pd.read_csv (abspath + '/data/summaries/summary_filter.csv', index_col=0)

plt.ion ()

df_true = df.loc[np.abs (df['Prot']-df['Prot_pred']) < 0.1 * df['Prot']] 
df_outliers = df.loc[np.abs (df['Prot_pred']-df['Prot']) > 0.1 * df['Prot']] 

fig = plt.figure (figsize=(14,12))
ax = fig.add_subplot (111)

ax.fill_between ([0,150], [0,135], [0,165], alpha=0.3, color='blue')
ax.plot ([0,150], [0,75], '--', linewidth=1., color='blue')

ax.set_ylim (-3, 120)

x = df_true['Prot'].to_numpy()
y = df_true['Prot_pred'].to_numpy()
xy = np.vstack([x, y])
z = gaussian_kde(xy)(xy)
idx = z.argsort()
x, y, z = x[idx], y[idx], z[idx]
z = z * df_true.index.size

scat = ax.scatter (x, y, marker='o', s=10, c=z, cmap='inferno')
cb = fig.colorbar (scat, ax=ax)
cb.set_label (label='Number of stars', size=28)
cb.ax.tick_params(labelsize='20')
#ax.scatter (df_true['Prot'], df_true['Prot_pred'], marker='o', s=10, color='white', edgecolor='black')
ax.scatter (df_outliers['Prot'], df_outliers['Prot_pred'], marker='o', s=10, color='white', edgecolor='red')

ax.set_xlabel (r'$P_\mathrm{rot,S19}$ (days)', fontsize=28)
ax.set_ylabel (r'$P_\mathrm{rot,ML}$ (days)', fontsize=28)

ax.tick_params (axis='both', labelsize=20)

plt.savefig (abspath + 'data/plot/distribution_period.pdf', format='pdf')



