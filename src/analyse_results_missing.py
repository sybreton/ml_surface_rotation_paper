import numpy as np
import pandas as pd
from pushkin.train import train_rf
from pushkin.classify import classify
from os import path
import sys
from tqdm import tqdm

abspath = path.abspath ('..')

rot = pd.read_csv (path.join (abspath,'data/summaries/missing_rot.csv'), index_col=0)
period = pd.read_csv (path.join (abspath,'data/summaries/missing_filter.csv'), index_col=0)
cpcb = pd.read_csv (path.join (abspath,'data/summaries/missing_cpcb.csv'), index_col=0)

print ('Number of classified stars with missing parameters :', rot.index.size)
print ('Number of classified rotating stars with missing parameters :', period.index.size)

print ('RotClass')
print ('Number of correctly classified stars with missing parameters :', 
       rot.loc[rot['label_pred']==rot['label_rot']].index.size)
print ('Corresponding fraction :', 
       rot.loc[rot['label_pred']==rot['label_rot']].index.size / rot.index.size)
print ('PeriodClass')
print ('Number of correctly labelled stars with missing parameters :', 
       period.loc[period['label_pred']==period['label_filter']].index.size)
print ('Corresponding fraction :', 
       period.loc[period['label_pred']==period['label_filter']].index.size / period.index.size)

print ('Number of period retrieved within 10% for stars with missing parameters :', 
       period.loc[period['diff'] < 0.1].index.size)
print ('Corresponding fraction :', 
       period.loc[period ['diff'] < 0.1].index.size / period.index.size)

print ('PollClass')
print ('Number of correctly classified stars with missing parameters :', 
       cpcb.loc[cpcb['label_pred']==cpcb['label']].index.size)
print ('Corresponding fraction :', 
       cpcb.loc[cpcb['label_pred']==cpcb['label']].index.size / cpcb.index.size)