import numpy as np
import pandas as pd
from pushkin.train import train_rf
from pushkin.classify import classify
from os import path
import sys
from tqdm import tqdm

n_train = 1
abspath = path.abspath ('..') + '/'
classify_missing = False

# SWITCH TO TRUE TO SAVE THE SUMMARY FRAME AND THE CLASSIFIER
summary = False
save = False
plot = True #will show only the first plot not to overhelm the notebook

PATH_OUT_CLASSIFIER = abspath + 'data/rf_storage/'
PATH_OUT = abspath + 'data/results_training/rotation/'
PATH_SAMPLES = abspath + '/data/input_samples/'
file_train = 'kepler_km_sample.csv'

file_missing = 'missing_sample.csv'
df_missing = pd.read_csv (PATH_SAMPLES+file_missing, index_col=0)

for ii in tqdm (range (n_train)) :

  if ii > 0 :
    plot = False

  df = pd.read_csv (PATH_SAMPLES+file_train, index_col=0)
  df = df.loc[np.setdiff1d (df.index, df_missing.index)]

  # Remove label columns
  input_param = df.columns.values
  input_param = np.setdiff1d (input_param, np.array(['Prot', 'CS_GAUSS_20', 'CS_GAUSS_55', 'CS_GAUSS_80', 'Sph_ACF_80', 'Sph_ACF_55',
                                                     'Prot_ACF_80', 'Sph_ACF_20', 'Prot_ACF_20', 'Prot_ACF_55',
                                                     'ACF_ER_SPH_20', 'ACF_ER_SPH_55', 'ACF_ER_SPH_80',
                                                    'label_rot']))
  df = df.loc[df['label_filter']>2] #remove elements for which the right rotation period is an unreachable parameter.

    
  #input_param = np.setdiff1d (input_param, np.array(['Prot', 'CS_GAUSS_20', 'CS_GAUSS_55', 'CS_GAUSS_80', 'Sph_ACF_80',
  #                                                   'Prot_ACF_80', 
  #                                                   'ACF_ER_SPH_80',
  #                                                   'label_rot']))
  #df = df.loc[df['label_filter']!=2] #remove elements for which the right rotation period is an unreachable parameter.


  df = df[input_param]

  # taking care of missing values
  df = df.replace (to_replace=-1.0, value=np.nan)
  df = df.replace (to_replace=-999.0, value=np.nan)
  df = df.replace (to_replace=np.inf, value=np.nan)
  df = df.replace (to_replace=-np.inf, value=np.nan)
  df = df.replace (to_replace='NaN', value=np.nan)
  df = df.dropna ()
  print (df.index.size)

  df = df.rename(columns={'label_filter':'label'})

  class_weight = 'balanced'
  test_size = 0.01
  
  clf = train_rf (df, clf_name=None, frame_name=None,
            n_estimators=300, criterion='gini', min_samples_split=2, plot=plot,
            class_weight=class_weight, test_size=test_size, summary=summary, save=save,
            random_state=ii, verbose=1)


ref = pd.read_csv (PATH_SAMPLES+file_train, index_col=0)    
    
input_param = np.setdiff1d (input_param, np.array(['label_filter'])) 
df_missing = df_missing.loc[np.intersect1d(df_missing.index, ref.loc[~ref['Prot'].isna()].index)]
df_missing = df_missing[input_param]
results = classify (df_missing, clf=clf, summary=False)


results = results.join (ref['label_filter'])

aux = df[np.intersect1d(df.columns, ['0.0', '1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '8.0'])].to_numpy ()
values = np.array (['Prot_ACF_20', 'Prot_ACF_55', 'Prot_ACF_80',
         'Prot_CS_20', 'Prot_CS_55', 'Prot_CS_80', 'Prot_GWPS_20',
         'Prot_GWPS_55', 'Prot_GWPS_80'])

results = results.join (ref[values])
results = results.join (ref['Prot'])
pos_prot = results[values].to_numpy ()
index = results['label_pred'].to_numpy ()
prot_pred = np.full (results.index.size, -9999.)
index = index.astype (np.float_)
index = index.astype (np.int_)
for ii, indprot in enumerate (index) :
    prot_pred[ii] = pos_prot[ii, indprot]
prot = results['Prot'].to_numpy()
diff = np.abs (prot_pred-prot)
results['Prot_pred'] = prot_pred
results['diff'] = diff
cond = diff > 0.1 * prot

results.to_csv (abspath+'data/summaries/missing_filter.csv')

