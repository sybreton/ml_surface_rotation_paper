import numpy as np
import pandas as pd
from pushkin.train import train_rf
from pushkin.classify import classify
from os import path
import sys
from tqdm import tqdm

abspath = path.abspath ('..') 



PATH_SAMPLES = path.join (abspath, 'data/input_samples/')
file_train = 'kepler_km_sample.csv'

df = pd.read_csv (path.join (PATH_SAMPLES, file_train), index_col=0)
# Remove label columns
input_param = df.columns.values
input_param = np.setdiff1d (input_param, np.array(['Prot', 'CS_GAUSS_20', 'CS_GAUSS_55', 'CS_GAUSS_80',
                                                   'label_filter']))
df = df[input_param]


df = df.replace (to_replace=-1.0, value=np.nan)
df = df.replace (to_replace=-999.0, value=np.nan)
df = df.replace (to_replace=np.inf, value=np.nan)
df = df.replace (to_replace=-np.inf, value=np.nan)
df = df.replace (to_replace='NaN', value=np.nan)

aux = df.dropna ()
missing = df.loc[np.setdiff1d(df.index, aux.index)]

print ('Number of stars in the missing sample', missing.index.size)
print ('Number of stars with rotation signal in the missing sample', missing.loc[missing['label_rot']=='Prot'].index.size)
print ('Fraction', missing.loc[missing['label_rot']=='Prot'].index.size / missing.index.size)
print ('Fraction in the full sample', df.loc[df['label_rot']=='Prot'].index.size / df.index.size)
print ('Fraction in the RotSample', aux.loc[aux['label_rot']=='Prot'].index.size / aux.index.size)




missing.to_csv (path.join (PATH_SAMPLES, 'missing_sample.csv'))