## Installation

You will need the following packages to run the code:
- numpy
- pandas
- matplotlib
- tqdm
- pushkin (`git clone https://gitlab.com/sybreton/pushkin.git`)

## Run the code

Run the 'pipeline.ipynb' with Jupyter Lab or Jupyter Notebook to train the classifiers, create the summary frames and analyse the results.

You can change the number of trainings and other parameters directly in the files :
- km_rot_train.py
- km_cpcb_train.py
- km_filter_train.py